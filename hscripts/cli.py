import argparse
import sys
from hscripts.core.lint import Lint
from hscripts.core.test import Test

from colorama import init, Fore, Back, Style

init(convert=True)

def sintax_parser():
    """Parse command and arguments"""

    parser = argparse.ArgumentParser()
    sub_parsers = parser.add_subparsers(help="commands")

    # lint command
    lint = sub_parsers.add_parser('lint', help='Runs linter')
    lint.add_argument("--dir", "-d", help="specifies directory")
    lint.set_defaults(func=Lint())

    # test command
    test = sub_parsers.add_parser('test', help='Runs test suite')
    test.add_argument("--dir", "-d", help="specifies module")
    test.add_argument("--cov", "-c", action='store_true', help="specifies coverage tests")
    test.set_defaults(func=Test())

    return parser

def run(config_file) -> int:
    """Main script execution"""
    parser = sintax_parser()
    args = parser.parse_args()

    if not hasattr(args, 'func'):
        parser.print_help()
    else:
        returncode, msg = args.func(config_file, args)
        if returncode == 0:
            msg = (
                f"{Fore.GREEN}"
                + msg
                + f"{Style.RESET_ALL}"
            )
        elif returncode > 0:
            msg = (
                f"{Fore.WHITE}{Back.RED}"
                + msg
                + f"{Style.RESET_ALL}"
            )
        elif returncode == -1:
            msg = (
                f"{Fore.MAGENTA}"
                + msg
                + f"{Style.RESET_ALL}"
            )
        print(msg)
        sys.exit(returncode)
