import os
import importlib.util
import subprocess
import shlex
import configparser
from pathlib import Path, WindowsPath

requirements = [
    "colorama",
    "pylint",
    "pylint_django",
    "pytest",
    "pytest_cov",
    "pytest_django",
]

def path_to_string(path):
    if isinstance(path, WindowsPath):
        return str(path).replace("\\","\\\\")
    return str(path)

setattr(Path, "to_string", path_to_string)

class HScriptConfig():

    def __init__(self, special_cfg_file: Path = None):

        self.cfg = configparser.ConfigParser()
        self.base_dir = Path.cwd()
        self._cfg_file = ""
        if special_cfg_file:
            self.cfg_file = Path(special_cfg_file)
        else:
            self.cfg_file = self.base_dir.joinpath('.hscripts')

    @property
    def cfg_file(self):
        return self._cfg_file

    @cfg_file.setter
    def cfg_file(self, cfg_file):
        self._cfg_file = cfg_file
        self.base_dir = self.cfg_file.parent
        self.cfg.read(self.cfg_file)

    def add_option(self, section: str, option: str, value: str):
        try:
            self.cfg.set(section, option, value)
            with open(self.cfg_file, 'w') as configfile:
                self.cfg.write(configfile)
        except FileNotFoundError as error:
            err_msg = (
                "hscript configuration file not found.\n"
                + "You should first call get_or_create()."
            )
            raise FileNotFoundError(err_msg) from error
        except configparser.NoSectionError:
            self.cfg.add_section(section)
            self.add_option(section, option, value)

    def get_or_create_option(
            self,
            section: str,
            option: str,
            default_value: str = None
          ):
        try:
            return self.cfg.get(section, option)
        except (
            configparser.NoSectionError,
            configparser.NoOptionError
        ) as error:
            if default_value:
                self.add_option(section, option, default_value)
                return default_value
            else:
                if error is configparser.NoSectionError:
                    error_name = "NoSectionError"
                else:
                    error_name = "NoOptionError"
                err_msg = (
                    f"configparser'{error_name}' exception raised "
                    + "and no default_value provided"
                )
                raise KeyError(err_msg) from error

    def get_or_create(self):

        if not self.cfg_file.exists():
            self.cfg_file.touch()
        
        base_dir = Path(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
        version_file = base_dir / "VERSION"
        if version_file.exists():
            with open(version_file, "r", encoding="utf-8") as fh:
                last_version = fh.read()
        else:
            pkg_dist_dirs = base_dir.glob("*hscripts*.dist*")
            for pkg_dist_dir in pkg_dist_dirs:
                metadata_file = pkg_dist_dir / "METADATA"
                with open(metadata_file, "r", encoding="utf-8") as fh:
                    for line in fh:
                        if line.startswith("Version"):
                            last_version = line.replace("Version: ","")
                break
            
        version = self.get_or_create_option(
            "Base",
            "version",
            default_value = last_version
        )
        if not version == last_version:
            msg = (
                "Conguration file for hscripts needs update.\n"
                + f"Configuration version: {version}\n"
                + f"Installed version: {last_version}"
            )
            print(msg)

        # Ensure Basic options

        ## Lint
        linter = self.get_or_create_option(
            "Lint",
            "pkg",
            default_value = "pylint"
        )
        if linter == "pylint":
            rcfile = self.get_or_create_option(
                "Lint",
                "rcfile",
                default_value = ".pylintrc"
            )
            rcfile_path = self.base_dir.joinpath(rcfile)
            if not rcfile_path.exists():
                rcfile_path.touch()
            self.get_or_create_option(
                "Lint",
                "cmd",
                default_value = "$pkg $dir --rcfile=$rcfile"
            )

        ## Test
        tester = self.get_or_create_option(
            'Test',
            'pkg',
            default_value = 'pytest'
        )
        if tester == "pytest":
            self.get_or_create_option(
                'Test',
                'plugins',
                default_value = 'pytest_cov'
            )
            config = self.get_or_create_option(
                "Test",
                "config",
                default_value = ".coveragerc"
            )
            config_path = self.base_dir.joinpath(config)
            if not config_path.exists():
                config_path.touch()
            if config == ".coveragerc" and config_path.stat().st_size < 8:
                with open(config_path, "r+") as f:
                    f.truncate()
                cfg_ = configparser.ConfigParser()
                cfg_.read(config_path)
                # run - branch = True
                cfg_.add_section("run")
                cfg_.set("run", "branch", "True")
                # report - show_missing = True
                cfg_.add_section("report")
                cfg_.set("report", "show_missing", "True")
                # report - fail_under = 70%
                cfg_.set("report", "fail_under", "70")
                # report - omit
                list_ = [
                    "*/venv/*",
                    "*/.venv/*",
                    "*/tests/*",
                    "*/*__init__.py",
                    "*/*setup.py"
                ]
                cfg_.set(
                    "report",
                    "omit", 
                    "\n"+"\n".join(list_)
                )
                # report - exclude_lines
                list_ = [
                    "pragma: no cover",
                    "raise NotImplementedError",
                ]
                cfg_.set(
                    "report",
                    "exclude_lines", 
                    "\n"+"\n".join(list_)
                )
                with open(config_path, "r+") as f:
                    cfg_.write(f)
            self.get_or_create_option(
                "Test",
                "cmd-cov",
                default_value = "--cov=. --cov-config=$config"
            )
            self.get_or_create_option(
                "Test",
                "cmd",
                default_value = "$pkg $dir $cov"
            )

        return self

    def cmd_generator(self, section: str, extra: dict = None) -> str:
        cmd = self.get_or_create_option(section, "cmd")
        while True:
            ini = cmd.find("$", 0)
            if ini == -1:
                break
            end_space = cmd.find(" ", ini)
            if end_space == -1:
                end_space = len(cmd)
            end_comma = cmd.find(",", ini)
            if end_comma == -1:
                end_comma = len(cmd)
            end = min(end_space, end_comma)
            param_name = cmd[(ini+1):end]
            if extra and param_name in extra.keys():
                param_value = extra.get(param_name)
                if param_value.startswith("$"):
                    param_value = self.get_or_create_option(
                        section,
                        param_value[1:]
                    )
            else:
                param_value = self.get_or_create_option(section, param_name)
            cmd = cmd.replace("$"+param_name,param_value)
        return cmd


def check_requirements_file(
        file: str,
        requirement: str,
        config_file: HScriptConfig
      ) -> int:
    path = config_file.base_dir.joinpath(file)
    if not path.exists():
        return 2
    else:
        with open(path) as f:
            for line in f:
                if requirement in line:
                    return 0
    return 1

def add_to_requirements_file(
        file: str,
        requirement: str,
        config_file: HScriptConfig,
        create_file: bool = False
      ):

    path = config_file.base_dir.joinpath(file)

    if not path.exists():
        if create_file:
            path.touch()
            return add_to_requirements_file(file, requirement, config_file)
        else:
            raise FileNotFoundError

    new_content = ""
    with open(path, "r+") as f:
        for line in f:
            if requirement in line:
                return "skiped"
            if not line == "":
                new_content += f"{line.rstrip()}\n"
        new_content += f"{requirement}\n"
        f.seek(0)
        f.truncate()
        f.write(new_content)
    return "added"

def check_packages(config_file: HScriptConfig):
    missing_pkgs = []
    for requirement in requirements:
        installed_package = importlib.util.find_spec(requirement)
        if installed_package is None:
            missing_pkgs.append(requirement)
    if len(missing_pkgs) > 0:
        print(f"hscripts missing dependencies:\n"+", ".join(missing_pkgs))
        # Install missing dependencies
        input_msg: str = (f"Would you like to install these packages now? (y/n)")
        input_ = input(input_msg)
        if input_ in ('y','Y'):
            cmd = "pip install "+", ".join(missing_pkgs)
            subprocess.run(shlex.split(cmd), check=True)
        else:
            return False
        # Add missing dependencies to requirements_dev.txt
        input_msg: str = (
            "Would you like to add to requirements_dev.txt "
            "all these newly installed packages? (y/n)"
        )
        input_ = input(input_msg)
        if input_ in ('y','Y'):
            for pkg in missing_pkgs:
                add_to_requirements_file(
                    "requirements_dev.txt",
                    pkg,
                    config_file,
                    create_file = True
                )
    return True
