from pathlib import Path
from hscripts.core.shell import ShellCommand

class Lint(ShellCommand):
    """Runs app linter"""

    NAME = "lint"
    CMD = ""
    OK_MSG = "No lint errors found"
    NOK_MSG = "Lint errors found! Please review your lint warnings."

    def __call__(self, config_file, args=None):
        if args.dir:
            params = {
                "dir": args.dir
            }
        else:
            params = {
                "dir": config_file.base_dir.to_string()
            }
        self.__class__.CMD = config_file.cmd_generator("Lint", extra=params)
        # Because several linters demand the starting folder to be a package-like
        # folder, if an __init__.py doesn't exist in the config_file.base_dir
        # then we'll create it temporarily (and delete it after linter execution)
        init_path = config_file.base_dir.joinpath("__init__.py")
        if init_path.exists():
            tmp_init = False  # Flag: __init__.py already existed
        else:
            init_path.touch()
            tmp_init = True  # Flag: __init__.py created

        returncode, msg = super().__call__(args)

        if tmp_init:  # Delete temporary __init__.py
            init_path.unlink()

        return returncode, msg
