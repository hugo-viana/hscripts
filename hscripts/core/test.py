from hscripts.core.shell import ShellCommand

class Test(ShellCommand):
    """Runs app tests"""

    NAME = "test"
    CMD = ""
    OK_MSG = "No test errors found"
    NOK_MSG = "Test errors found! Please review your test output."

    def __call__(self, config_file, args=None):
        params = {}
        if args.dir:
            params["dir"] = args.dir
        else:
            params["dir"] = config_file.base_dir.to_string()
        if args.cov:
            params["cov"] = "$cmd-cov"
        else:
            params["cov"] = ""
        self.__class__.CMD = config_file.cmd_generator("Test", extra=params)
        return super().__call__(args)