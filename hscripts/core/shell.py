import subprocess
import shlex

class ShellCommand():
    """Serves as interface for executing shell commands"""

    NAME = None
    CMD = None
    OK_MSG = None
    NOK_MSG = None

    def __call__(self, args=None):
        cmd = self.__class__.CMD
        try:
            process = subprocess.run(shlex.split(cmd), check=True)
            returncode = process.returncode
            msg = self.__class__.OK_MSG
        except ValueError as error:
            msg = f"Error while calling '{self.__class__.NAME}'."
            msg += f"\nValueError:\n{error}"
            returncode = 1
        except OSError as error:
            msg = f"Error while calling '{self.__class__.NAME}'."
            msg += f"\nOSError:\n{error}"
            returncode = 1
        except subprocess.TimeoutExpired as error:
            msg = f"Error while calling '{self.__class__.NAME}'."
            msg += f"\nTimeoutExpired:\n{error}"
            returncode = 1
        except (subprocess.CalledProcessError, subprocess.SubprocessError):
            msg = self.__class__.NOK_MSG
            returncode = -1
        return returncode, msg