import os
from hscripts.config import check_packages, HScriptConfig

def launch():
    cfg = HScriptConfig().get_or_create()
    if check_packages(cfg):
        from hscripts import cli
        cli.run(cfg)
    else:
        err_msg: str = (
            "Cannot run hscripts due to unsufficient "
            + "installed depedency packages"
        )
        print(err_msg)
