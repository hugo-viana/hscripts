# hscripts

Welcome to the hscripts project


## Instalation

```shell
pip install hscripts
```


## Usage

TODO


## Colaborate

1. Fork the [hscripts repo](https://gitlab.com/hugo-viana/hscripts) and clone it
into your development workspace.

2. Create virtual environment and activate it:

    ```shell
    py -m venv .venv
    .venv\Scripts\Activate.ps1
    ```

3. Install package setup dependencies:

    ```shell
    pip install --upgrade --force-reinstall -r requirements_pkg.txt
    ```

4. Make code changes / improvements / fixes.

5. Create sufficient unit-tests for all changes

6. Successfully run all tests

7. Build package

    ```shell
    py setup.py sdist bdist_wheel
    ```

8. Create test account in [testpypi](https://test.pypi.org/)

9. Upload to testpypi the new package [(more info)](https://packaging.python.org/tutorials/packaging-projects/#uploading-the-distribution-archives)

    ```shell
    py -m twine upload --repository testpypi dist/*
    ```

10. Test it with a real project, installing hscripts with pip install [(more info)](https://packaging.python.org/tutorials/packaging-projects/#installing-your-newly-uploaded-package)

11. Push changes to [hscripts repo](https://gitlab.com/hugo-viana/hscripts)


## References

- [Python Packaging Projects Tutorial](https://packaging.python.org/tutorials/packaging-projects/)
- [Test PyPI website](https://test.pypi.org/)