import setuptools
from hscripts.config import requirements as hs_dependencies

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

with open("VERSION", "r", encoding="utf-8") as fh:
    version = fh.read()

setuptools.setup(
    name="hscripts",
    version=version,
    author="Hugo Viana",
    author_email="hugosemianoviana@gmail.com",
    description="Helper scripts for python project operations",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/hugo-viana/hscripts",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=hs_dependencies,
    entry_points={
        'console_scripts': [
            'hs=hscripts.launcher:launch'
        ]
    }
)
